import firebase from 'firebase/app'

export const state = () => ({
  currentUser: null,
  account: []
})

export const getters = {
  getCurrentUser: state => {
    return state.currentUser
  },
  getAccount: state => {
    return state.account
  }
}

export const actions = {
  async signIn ({ commit }, form) {
    return new Promise(function (resolve, reject) {
      firebase.auth().signInWithEmailAndPassword(form.email, form.password)
        .then(function (user) {
          resolve(user)
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
  async register ({ commit }, form) {
    return new Promise(function (resolve, reject) {
      firebase.auth().createUserWithEmailAndPassword(form.email, form.password)
        .then(async function() {
          let user = firebase.auth().currentUser
          user.updateProfile({
            displayName: form.displayName,
          })
          let db = firebase.firestore()
          await db.collection('users').doc(user.uid).set({
            displayName: form.displayName,
            account: []
          })
          await firebase.auth().signOut()
          resolve(user)
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
  async fetchAccount ({ commit }) {
    let db = firebase.firestore()
    let user = firebase.auth().currentUser
    db.collection('users').doc(user.uid).onSnapshot(function (data) {
      let account = data.data().account
      if (!account) {
        account = []
      }
      commit('setAccount', account)
    })
  },
  addAccount ({ commit }, form) {
    return new Promise(async function (resolve, reject) {
      let db = firebase.firestore()
      let user = firebase.auth().currentUser
      let data = await db.collection('users').doc(user.uid).get()
      let account = data.data().account
      if (!account) {
        account = []
      }
      form.id = db.collection('users').doc().id
      account.push(form)
      await db.collection('users').doc(user.uid).update({
        account: account
      })
      resolve()
    })
  },
  deleteAccount ({ commit }, item) {
    return new Promise(async function (resolve, reject) {
      let db = firebase.firestore()
      let user = firebase.auth().currentUser
      let data = await db.collection('users').doc(user.uid).get()
      let account = data.data().account
      account = account.filter(a => a.id !== item.id)
      await db.collection('users').doc(user.uid).update({
        account: account
      })
      resolve()
    })
  }
}

export const mutations = {
  setUser (state, user) {
    state.currentUser = JSON.parse(JSON.stringify(user))
  },
  setAccount (state, account) {
    state.account = account
  }
}