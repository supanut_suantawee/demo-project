export default ({ app }) => {
  app.router.beforeEach(async function (to, from, next) {
    if (isAdminRoute(to) || to.name === 'index') {
      app.store.dispatch('setNavStatus', false)
    } else {
      app.store.dispatch('setNavStatus', true)
    }
    next()
  })
}

function isAdminRoute (route) {
  if (route.matched[0].name.includes('login')) {
    return true
  }
  if (route.matched[0].name.includes('register')) {
    return true
  }
  return false
}