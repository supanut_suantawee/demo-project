import firebase from 'firebase/app'

export default async function ({ store, redirect, route }) {
  let currentUser = firebase.auth().currentUser
  if (currentUser === null && isAdminRoute(route)) {
    return redirect('/login')
  }
  if (currentUser !== null && route.name === 'login') {
    return redirect('/')
  }
}
function isAdminRoute (route) {
  if (route.matched.some(record => record.path === '/home')) {
    return true
  }
  if (route.matched.some(record => record.path === '/account')) {
    return true
  }
  return false
}