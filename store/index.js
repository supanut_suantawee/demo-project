export const state = () => ({
  navStatus: false,
  currentUser: null
})

export const getters = {
  getNavStatus: state => {
    return state.navStatus
  },
  getCurrentUser: state => {
    return state.currentUser
  }
}

export const actions = {
  async setNavStatus ({ commit }, val) {
    commit('setNavStatus', val)
  },
  setCurrentUser ({ commit }, user) {
    commit('setCurrentUser', user)
  }
}

export const mutations = {
  setNavStatus (state, status) {
    state.navStatus = JSON.parse(JSON.stringify(status))
  },
  setCurrentUser (state, user) {
    state.currentUser = JSON.parse(JSON.stringify(user))
  }
}