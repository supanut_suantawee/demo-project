import firebase from 'firebase/app'
import "firebase/auth"
import "firebase/database"
import "firebase/firestore"

var config = {
  apiKey: 'AIzaSyCX4_WSmbEwnsxzZX7pEiTmoIVjOGKpZ6g',
  authDomain: 'fir-project-d67fb.firebaseapp.com',
  databaseURL: 'https://fir-project-d67fb.firebaseio.com',
  projectId: 'fir-project-d67fb',
  storageBucket: 'fir-project-d67fb.appspot.com',
  messagingSenderId: '283572827883',
  appId: '1:283572827883:web:995e651e64d9eb52'
}

firebase.initializeApp(config)

export default (context) => {
  const {store} = context

  return new Promise((resolve, reject) => {
    firebase.auth().onAuthStateChanged(function (user) {
      context.store.dispatch('setCurrentUser', user)
      resolve()
    })
  })
}
